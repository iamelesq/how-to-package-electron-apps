# packaging electron apps

method uses package `electron-packager` and adds scripts to the `package.json` for a project.

```json
{
    "name": "repo-name",
    "productName": "product-name",
    "version": "version",
    "description": "what I do",
    "main": "main.js",
    "scripts": {
        "start": "electron .",
        "dev": "nodemon --exec electron .",
        "package-mac": "electron-packager . --overwrite --platform=darwin --arch=x64 --icon=assets/icons/mac/icon.icns --prune=true --out=release-builds",
        "package-win": "electron-packager . --overwrite --platform=win32 --arch=ia32 --icon=assets/icons/win/icon.ico --prune=false --out=release-builds --version-string.CompanyName=CE --version-string.FileDescription=CE --version-string.ProductName=\"ImageShrink\"",
        "package-linux": "electron-packager . --overwrite --platform=linux --arch=x64 --icon=assets/icons/png/1024x1024.png --prune=false --out=release-builds"
    },
    "keywords": [],
    "author": "elesq",
    "license": "ISC",
    "devDependencies": {
        "electron": "^8.2.5",
        "electron-packager": "^15.4.0"
        // dependencies
    },
    "dependencies": {
        // dependencies
    }
}
```
